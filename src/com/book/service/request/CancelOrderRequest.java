package com.book.service.request;

public class CancelOrderRequest {
	
	private int _orderId;
	
	public int getOrderId(){
		return _orderId;
	}
	
	public void setOrderId(int orderId) {
		_orderId = orderId;
	}
}
