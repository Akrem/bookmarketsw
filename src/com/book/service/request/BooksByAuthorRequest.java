package com.book.service.request;

public class BooksByAuthorRequest {
	private String _author;
	public String getAuthor(){
		return _author;
	}
	public void setAuthor(String author){
		_author = author;
	}
}
