package com.book.service.request;

public class OrderStatusRequest {
	private int _orderId;
	
	public int getOrderId(){
		return _orderId;
	}
	
	public void setOrderId(int orderId) {
		_orderId = orderId;
	}
}
