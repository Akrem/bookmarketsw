package com.book.service.request;

public class BookRequest {
	private long _isbn;
	
	public BookRequest() {}
	
	public long getIsbn(){
		return _isbn;
	}
	
	public void setIsbn(long isbn) {
		_isbn = isbn;
	}
	
}
