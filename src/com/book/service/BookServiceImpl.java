package com.book.service;

//import java.util.ArrayList;

import java.util.Date;

import javax.jws.WebService;

import com.book.business.*;
import com.book.data.BookStoreRepository;
import com.book.service.request.*;

@WebService(endpointInterface = "com.book.service.BookService", serviceName = "BookServices")
public class BookServiceImpl implements BookService {
	
	private BookStoreRepository bookStoreRepository;
	
	public BookServiceImpl(){
		bookStoreRepository = BookStoreRepository.Instance();
	}
	
	public Book[] searchBooks(BooksByAuthorRequest booksRequest) {
		return bookStoreRepository.searchBooks(booksRequest.getAuthor());
	}
	
	public Book searchBook(BookRequest bookRequest) {
		return bookStoreRepository.searchBook(bookRequest.getIsbn());
	}
	
	public int buyBook(OrderRequest orderRequest) {
		return bookStoreRepository.buyBook(orderRequest.getIsbn(), orderRequest.getShippingAddress(), orderRequest.getPayment());
	}
	
	public String orderStatus(OrderStatusRequest orderStatusRequest) {
		return bookStoreRepository.orderStatus(orderStatusRequest.getOrderId());
	}
	
	public boolean cancelOrder(CancelOrderRequest cancelOrderRequest) {
		return bookStoreRepository.cancelOrder(cancelOrderRequest.getOrderId());
	}

}
