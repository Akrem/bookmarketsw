package com.book.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.book.business.*;
import com.book.service.request.*;

@WebService
public interface BookService {

	@WebResult(targetNamespace="", name="Books")
	public Book[] searchBooks(@WebParam(name = "BooksRequest") BooksByAuthorRequest booksRequest);

	@WebResult(targetNamespace="", name="Book")
	public Book searchBook(@WebParam(name = "BookRequest") BookRequest bookRequest);
	
	@WebResult(targetNamespace="", name="OrderId")
	public int buyBook(@WebParam(name="OrderRequest") OrderRequest orderRequest);
	
	@WebResult(targetNamespace="", name="OrderId")
	public String orderStatus(@WebParam(name="OrderStatusRequest") OrderStatusRequest orderStatusRequest);
	
	@WebResult(targetNamespace="", name="CancelOrder")
	public boolean cancelOrder(@WebParam(name="CancelOrderRequest") CancelOrderRequest cancelOrderRequest);
	

}