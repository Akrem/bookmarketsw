package com.book.business;

public enum BookStatus {
	Available,
	Sold
}
