package com.book.business;

public enum OrderStatus {
	Ordered,
	Shipped,
	Canceled	
}
