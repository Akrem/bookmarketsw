package com.book.business;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Book implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long _isbn;
	private String _title;
	private String _author;
	private double _price;
	private Date _publishedDate;
	private BookStatus _status;
	
	public Book(){}
	
	public Book(long isbn, String title, String author, double price, Date publishedDate, BookStatus bookStatus) 
	{
		_isbn = isbn;
		_title = title;
		_author = author;
		_publishedDate = publishedDate;
		_price = price;
		_status = bookStatus;
	}


	public long getIsbn() {
		return _isbn;
	}
	
	public String getTitle() {
		return _title;
	}
	public void setTitle(String _title) {
		this._title = _title;
	}

	public String getAuthor() {
		return _author;
	}

	public Date getPublishedDate() {
		return _publishedDate;
	}

	public void setPublishedDate(Date _publishedDate) {
		this._publishedDate = _publishedDate;
	}

	public double getPrice() {
		return _price;
	}

	public void setPrice(double _price) {
		this._price = _price;
	}

	public BookStatus getStatus() {
		return _status;
	}

	public void setStatus(BookStatus _status) {
		this._status = _status;
	}
}

