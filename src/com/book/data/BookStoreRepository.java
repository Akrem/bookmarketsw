package com.book.data;

import java.util.Date;

import com.book.business.Book;
import com.book.business.BookStatus;
import com.book.business.Order;
import com.book.business.OrderStatus;

public class BookStoreRepository {
	
	private static BookStoreRepository _bookStoreRepository = null;
	private static Book[] _books;
	private static int MaxBookIndex;
	
	private static Order[] _orders;
	private static int MaxOrderIndex;
	
	public static BookStoreRepository Instance(){
		if (_bookStoreRepository == null) {
			_bookStoreRepository = new BookStoreRepository();
			
		}
		
		return _bookStoreRepository;
	}
	
	private BookStoreRepository(){
		_books = new Book[100];
		for(int i = 0; i<100; i++)
			_books[i] = null;
		MaxBookIndex = 1;
		
		_orders = new Order[100];
		for(int i = 0; i<100; i++)
			_orders[i] = null;
		MaxOrderIndex = 1;
		

		Book book1 = new Book(1, "Dark Night", "James Fox", 12.50, new Date(01, 01, 1984), BookStatus.Available);
		Book book2 = new Book(2, "Happy Day", "James Fox", 12.50, new Date(01, 01, 1984), BookStatus.Available) ;
		Book book3 = new Book(3, "Next Morning", "Renee Washinton", 55.50, new Date(01, 01, 1984), BookStatus.Available);
		
		addBook(book1);
		addBook(book2);
		addBook(book3);
	}
	
	public void addBook(Book book) {
		_books[MaxBookIndex++] = book;
	}
	
	public Book[] searchBooks(String author) {
		Book[] result = new Book[100];
		int maxIndex = 1;
		for (int i=1; i<100; i++) {
		  if (_books[i]!=null && _books[i].getAuthor().equals(author)) {
			  result[maxIndex++] = _books[i];
		  }
		}
		return result;
	}
	
	public Book searchBook(long isbn) {
		for (int i = 1; i < 100; i++) {
			  if (_books[i] != null && _books[i].getIsbn() == isbn) {
			    return _books[i];
			  }
		}
		return null;
	}
	
	public Book[] getAllBooks() {
		return _books;
	}
	
	public int buyBook(long isbn, String shipping, double payment) {
		Book book = searchBook(isbn);
		
		if (book == null || !book.getStatus().equals(BookStatus.Available)) {
			return -1;
		}
		book.setStatus(BookStatus.Sold);
		Order order = new Order(MaxOrderIndex, isbn, shipping, payment);
		order.setOrderStatus(OrderStatus.Ordered);
		_orders[MaxOrderIndex++] = order;
		return order.getOrderId();
	}
	
	public boolean cancelOrder(int orderId) {
		Order order = _orders[orderId];
		
		if (order == null || !order.getOrderStatus().equals(OrderStatus.Ordered)) {
			return false;
		}
		
		Book orderedBook = searchBook(order.getIsbn());
		orderedBook.setStatus(BookStatus.Available);
		order.setOrderStatus(OrderStatus.Canceled);
		return true;
			
	}
	
	public String orderStatus(int orderId) {
		return _orders[orderId].getOrderStatus().toString();
	}
}
