package com.book.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.book.business.Book;
import com.book.service.BookService;
import com.book.service.request.BookRequest;
import com.book.service.request.*;

public final class BookServiceClient {

    private BookServiceClient() {
    } 

    public static void main(String args[]) throws Exception {
    	JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

    	factory.getInInterceptors().add(new LoggingInInterceptor());
    	factory.getOutInterceptors().add(new LoggingOutInterceptor());
    	
    	factory.setBindingId("http://cxf.apache.org/bindings/xformat");
    	
    	factory.setServiceClass(BookService.class);
    	factory.setAddress("http://localhost:8080/BookMarketWS/services/cxfBookService");
    	BookService bookService = (BookService) factory.create();

    	//Now, start sending requests and accepting responses
    	BookRequest bookRequest = new BookRequest();
    	bookRequest.setIsbn(3);
    	Book book = bookService.searchBook(bookRequest);
    	System.out.println("The book title is: " + book.getTitle() + ", Author: "+ book.getAuthor() + ", " + book.getStatus());
    	
    	System.out.println("The book search results:");
    	BooksByAuthorRequest booksRequest = new BooksByAuthorRequest();
    	booksRequest.setAuthor("James Fox");
    	Book[] books = bookService.searchBooks(booksRequest);
    	for(int i=1; i<books.length && books[i]!=null; i++){
    		System.out.println("The book title is: " + books[i].getTitle() + ", " + books[i].getAuthor() + ", " + books[i].getStatus());
    	}
    	
    	// Buy a book
    	System.out.println("Buy a book:");
    	System.out.println("isbn = 3:");
    	OrderRequest orderRequest = new OrderRequest();
    	orderRequest.setIsbn(3);
    	
    	orderRequest.setShippingAddress("123 N Howard Chicago, IL 60660");
    	orderRequest.setPayment(55.50);
    	int orderId = bookService.buyBook(orderRequest);
    	System.out.println("Order Number = " + orderId);
    	bookRequest.setIsbn(3);
    	Book orderedBook = bookService.searchBook(bookRequest);
    	System.out.println("Current Status of the book: " + orderedBook.getStatus());
    	OrderStatusRequest osr = new OrderStatusRequest();
    	osr.setOrderId(orderId);
    	System.out.println("Current Status of the order: " + bookService.orderStatus(osr));
    	
    	// Cancel Order
    	// Buy a book
    	System.out.println("Cancel order:");
    	System.out.println("Order Number = " + orderId);
    	CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
    	cancelOrderRequest.setOrderId(orderId);
    	
    	boolean success = bookService.cancelOrder(cancelOrderRequest);
    	System.out.println("Cancel Order Number = " + orderId + ". Success = " + success);
    	bookRequest.setIsbn(3);
    	orderedBook = bookService.searchBook(bookRequest);
    	System.out.println("Current Status of the book: " + orderedBook.getStatus());
    	osr = new OrderStatusRequest();
    	osr.setOrderId(orderId);
    	System.out.println("Current Status of the order: " + bookService.orderStatus(osr));
    	
    	System.exit(0);

    }

}